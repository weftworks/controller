# Weftworks SDN Solution
# Copyright (C) 2018 Antti Lohtaja
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

function(assert_build_directory)
  get_filename_component(SOURCE_DIR "${CMAKE_SOURCE_DIR}" REALPATH)
  get_filename_component(BINARY_DIR "${CMAKE_BINARY_DIR}" REALPATH)

  if(NOT ${SOURCE_DIR}/build STREQUAL ${BINARY_DIR})
    string(ASCII 27 Esc)
    message("${Esc}[1m")
    message(" In-source builds are not allowed. ")
    message(" Remove the CMakeCache.txt file and run cmake .. inside build/ directory.\n ")
    message(" Example: ")
    message(" $ rm CMakeCache.txt ")
    message(" $ mkdir build ")
    message(" $ cd build ")
    message(" $ cmake .. ")
    message(" $ make ")
    message(" ${Esc}[m ")
    message(FATAL_ERROR "")
  endif()
endfunction()

assert_build_directory()

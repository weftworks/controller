/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <thread>

#include <boost/thread/thread.hpp>

#include <weftworks/cli.hpp>
#include <weftworks/logger.hpp>

#include "build_version.h"
#include "controller/controller.hpp"

int main(int argc, char** argv)
{
        /* set and store the command line options */
        weftworks::cli::options options;
        options.add<std::string>("config,c", "set config file");
        options.add<std::string>("debug,d", "set logger debug level");
        options.add<uint16_t>("threads,t", "set amount of threads to use");
        options.add<uint16_t>("of-port", "set openflow port [6653]");
        options.store(argc, argv);

        /* check if version option was used */
        if (options.is_set("version")) {
                std::cout << boost::format("Weftworks Controller version: %1%\n")
                        % WEFTWORKS_CONTROLLER_VERSION;
                std::cout << boost::format("Boost version: %1%.%2%.%3%\n")
                        % (BOOST_VERSION / 100000)
                        % (BOOST_VERSION / 100 % 1000)
                        % (BOOST_VERSION % 100);
                std::exit(EXIT_FAILURE);
        }

        /* set the logger severity level. */
        if (options.is_set("debug"))
                LOG_DEBUG("init", "Debug level was set to %s.", options.get_value<std::string>("debug"));
        else
                LOG_DEBUG("init", "Debug level was not set.");

        /* get the available thread count. */
        uint16_t thread_count = std::thread::hardware_concurrency();
        if (thread_count < 1)
                thread_count = 1;

        LOG_INFO("init", "Found %s available thread(s).", thread_count);

        /* set the thread count. */
        if (options.is_set("threads")) {
                uint16_t input = options.get_value<uint16_t>("threads");
                if (input > 0 and input <= thread_count)
                        thread_count = input;
                else
                        LOG_FATAL("init", "Invalid thread count, aborting.");
        } else {
                LOG_INFO("init", "Thread count was not set.");
                thread_count /= 2;
        }
        LOG_INFO("init", "Using %s thread(s).", thread_count);

        boost::asio::io_context io_context;
        boost::thread_group thread_pool;
        boost::asio::io_context::work work(io_context);

        /* create the controller. needs to be created before starting the thread pool */
        weftworks::controller controller(boost::ref(io_context));

        /* start the thread pool */
        for (int i = 0; i < thread_count; ++i) {
                thread_pool.create_thread(boost::bind(&boost::asio::io_context::run, &io_context));
        }

        LOG_INFO("init", "");
        LOG_INFO("init", "Weftworks SDN Controller - Copyright (C) 2018 Antti Lohtaja");
        LOG_INFO("init", "This program comes with ABSOLUTELY NO WARRANTY.");
        LOG_INFO("init", "This is free software, and you are welcome to redistribute it");
        LOG_INFO("init", "under certain conditions.");
        LOG_INFO("init", "<Ctrl-C> to stop the controller.");
        LOG_INFO("init", "");

        /* start the controller */
        controller.start();

        /* main loop */
        while (controller.is_running()) {
                boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
        }

        /* shutdown starts here */
        LOG_INFO("shutdown", "Waiting for threads to finish work..");
        io_context.stop();
        thread_pool.join_all();
        /* wait a second to finish all logging */
        boost::this_thread::sleep_for(boost::chrono::seconds(1));
        LOG_INFO("shutdown", "Goodbye.");

        return EXIT_SUCCESS;
}

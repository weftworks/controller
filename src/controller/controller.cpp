/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "controller/controller.hpp"

namespace weftworks {

controller::controller(boost::asio::io_context& io_context)
        : openflow_manager(std::make_shared<network::openflow::openflow_manager>(io_context))
        , signals(io_context)
        , state(controller_state::stopped)
{
        await_shutdown();
}

void controller::start()
{
        try {
                LOG_INFO("openflow", "Starting network.");
                if (not openflow_manager->start_network("0.0.0.0", 6653))
                        LOG_ERROR("openflow", "Could not start the network.");
                state = controller_state::running;
                LOG_INFO("controller", "Controller started.");
        } catch (std::exception &error) {
                LOG_ERROR("controller", "Error starting the controller: %s", error.what());
        }
}

void controller::shutdown()
{
        LOG_INFO("controller", "Shutting down.");
        if (openflow_manager->stop_network())
                state = controller_state::stopped;
}

void controller::pause()
{
        state = controller_state::paused;
        LOG_INFO("controller", "Controller paused.");
}

void controller::resume()
{
        state = controller_state::running;
}

bool controller::is_running()
{
        return state == controller_state::running;
}

void controller::await_shutdown()
{
        auto shutdown_handler = [this](boost::system::error_code error, int sig)
        {
                // line break just to make ctrl-c prettier
                std::cout << std::endl;
                if (error)
                        LOG_ERROR("shutdown", "Error trying to shutdown the controller: %s", error.message());
                else
                        shutdown();
        };
        signals.add(SIGINT);
        signals.add(SIGTERM);
        signals.async_wait(shutdown_handler);
}

} // namespace weftworks

/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "network/openflow/openflow_connection.hpp"

namespace weftworks::network::openflow {

openflow_connection::openflow_connection(boost::asio::io_context& io_context)
        : base::tcp_connection(io_context)
{
        LOG_DEBUG("openflow", "New connection created.");
}

void openflow_connection::start()
{
        if (is_closed())
        {
                LOG_ERROR("openflow", "Tried to start a connection on a closed or closing socket.");
                return;
        }
        LOG_DEBUG("openflow", "New connection started.");
        set_timeout();
        read();
}

void openflow_connection::handle_read(boost::system::error_code const& error, size_t bytes_transferred)
{
        if (error) {
                LOG_WARN("openflow", "Connection error: %s", error.message());
                return;
        }
        // Parse read data.
        weftworks::openflow::header request{0, 0, 0, 0};
        request_parser request_parser;
        boost::tribool result;
        std::tie(result, std::ignore) = request_parser.parse(
                request, get_read_buffer().data(), get_read_buffer().data() + bytes_transferred
        );
        if (result) {
                LOG_DEBUG("openflow", "bt %s", bytes_transferred);
                LOG_DEBUG("openflow", "version %s", request.version);
                LOG_DEBUG("openflow", "type %s", request.type);
                LOG_DEBUG("openflow", "length %s", request.length);
                LOG_DEBUG("openflow", "xid %s", request.xid);
        } else {
                LOG_WARN("openflow", "Invalid OpenFlow request received.");
                close_connection();
        }
}

} // namespace weftworks::network::openflow

/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "network/openflow/openflow_manager.hpp"

namespace weftworks::network::openflow {

namespace {
        namespace base = weftworks::network::base;
};

openflow_manager::openflow_manager(boost::asio::io_context& io_context)
        : base::network_manager(io_context)
{ }

void openflow_manager::handle_accept(std::shared_ptr<base::tcp_connection> connection)
{
        if (connection)
                connection->start();
        start_accept();
}

std::shared_ptr<base::tcp_connection> openflow_manager::create_accepting_connection(boost::asio::io_context& io_context)
{
        return std::make_shared<openflow_connection>(io_context);
}

} // namespace Weftworks::Network::OpenFlow

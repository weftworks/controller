/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WEFTWORKS_INCLUDE_NETWORK_OPENFLOW_OPENFLOW_SOCKET_HPP
#define WEFTWORKS_INCLUDE_NETWORK_OPENFLOW_OPENFLOW_SOCKET_HPP

#include <weftworks/network/base.hpp>
#include <weftworks/openflow/openflow14.hpp>
#include <weftworks/network/openflow/request_parser.hpp>

namespace weftworks::network::openflow {

/**
 *  represents an openflow connection
 */
class openflow_connection : public base::tcp_connection
{
public:
        /**
         *  openflow_connection constructor
         *  @param io_context boost asio io context
         */
        openflow_connection(boost::asio::io_context& io_context);
        /**
         *  start the data exchange
         */
        void start() override;
protected:
        /**
         *  handle data read from the socket
         *  @param error_code result of the read event
         *  @param bytes_transferred read data
         */
        void handle_read(boost::system::error_code const& error_code, size_t bytes_transferred) override;
};

} // Namespace Weftworks::Network::OpenFlow

#endif // WEFTWORKS_INCLUDE_NETWORK_OPENFLOW_OPENFLOW_SOCKET_HPP

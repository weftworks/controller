/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WEFTWORKS_CONTROLLER_INCLUDE_NETWORK_OPENFLOW_OPENFLOW_MANAGER_HPP
#define WEFTWORKS_CONTROLLER_INCLUDE_NETWORK_OPENFLOW_OPENFLOW_MANAGER_HPP

#include <weftworks/logger.hpp>
#include <weftworks/network/base.hpp>
#include <weftworks/network/openflow.hpp>

#include "openflow_connection.hpp"

namespace weftworks::network::openflow {

namespace {
        namespace base = weftworks::network::base;
};

/**
 *  handles openflow connections
 */
class openflow_manager : public base::network_manager
{
public:
        /**
         *  openflow_manager constructor
         *  @param io_context boost asio io context
         */
        openflow_manager(boost::asio::io_context& io_context);

        /**
         *  handle a new openflow connection
         *  @param connection pointer to a openflow_connection object
         */
        void handle_accept(std::shared_ptr<base::tcp_connection> connection);
        /**
         *  create a pointer to a new openflow_connection object
         *  used for accepting new openflow connections
         *  @param io_context boost asio io context
         *  @return pointer to a openflow_connection object
         */
        std::shared_ptr<base::tcp_connection> create_accepting_connection(boost::asio::io_context& io_context);
};

} // namespace weftworks::network::openflow

#endif // WEFTWORKS_CONTROLLER_INCLUDE_NETWORK_OPENFLOW_OPENFLOW_MANAGER_HPP

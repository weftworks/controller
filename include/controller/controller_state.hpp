/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WEFTWORKS_INCLUDE_CONTROLLER_CONTROLLER_STATE_HPP
#define WEFTWORKS_INCLUDE_CONTROLLER_CONTROLLER_STATE_HPP

#include <weftworks/utility.hpp>

namespace weftworks {

/**
 *  pure abstract base class for the controller
 */
class controller_state : utility::noncopyable
{
public:
        /** define how to start the controller */
        virtual void start() = 0;
        /** define how to shutdown the controller */
        virtual void shutdown() = 0;
        /** define how to pause the controller */
        virtual void pause() = 0;
        /** define how to resume a paused controller */
        virtual void resume() = 0;
};

} // namespace weftworks

#endif // WEFTWORKS_INCLUDE_CONTROLLER_CONTROLLER_STATE_HPP

/*
*  Weftworks SDN Solution
*  Copyright (C) 2018 Antti Lohtaja
*
*  This program is free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WEFTWORKS_INCLUDE_CONTROLLER_CONTROLLER_HPP
#define WEFTWORKS_INCLUDE_CONTROLLER_CONTROLLER_HPP

#include <boost/asio/io_context.hpp>
#include <boost/asio/signal_set.hpp>

#include <weftworks/logger.hpp>

#include "network/openflow/openflow_manager.hpp"
#include "controller_state.hpp"

namespace weftworks {

/**
 *  controller composite class
 */
class controller : public controller_state
{
public:
        /**
         *  controller constructor
         *  @param io_context boost asio io context
         */
        controller(boost::asio::io_context &io_context);
        /**
         *  start the controller
         */
        void start();
        /**
         *  stop network managers and set the controller state to 0, which stops the main loop
         */
        void shutdown();
        /**
         *  pause the controller
         */
        void pause();
        /**
         *  resume operations if the controller is paused
         */
        void resume();
        /**
         *  return true if the controller is running
         *  @return controller running status
         */
        bool is_running();
private:
        /**
         *  handle shutdown signals
         */
        void await_shutdown();

        /**
         *  current state of the controller
         */
        enum class controller_state
        {
                stopped         = 0,
                running         = 1,
                paused          = 2,
        };

        std::shared_ptr<network::openflow::openflow_manager> openflow_manager;
        boost::asio::signal_set signals;
        controller_state state;
};

} // namespace weftworks

#endif // WEFTWORKS_INCLUDE_CONTROLLER_CONTROLLER_HPP
